json.array!(@products) do |product|
  json.extract! product, :id, :categoria, :subcategoria, :descripcion, :stock, :atributo, :precio, :imagen
  json.url product_url(product, format: :json)
end
