class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :categoria
      t.string :subcategoria
      t.string :descripcion
      t.integer :stock
      t.string :atributo
      t.float :precio
      t.binary :imagen

      t.timestamps
    end
  end
end
